import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.util.ArrayList;

public class Supervision extends JFrame {


    private JTextArea txtCajsGrandes;
    private JTextArea txtCajasMedianas;
    private JTextArea txtCajasPequeñas;
    private JLabel CajasGrandes;
    private JLabel CajasMedianas;
    private JLabel CajasPequenyas;
    private JPanel jPanel;
    private JLabel lblCajamasClientesGestionados;
    private JLabel CajaMenosTiempo;
    private JLabel lblMaxClientes;
    private JLabel lblMenosTiempo;
    private JLabel ClienteMasTiempo;
    private JLabel lblCajaMaxTime;
    private JLabel mercadonaIcon;
    private JButton btnGetQueryspecial;




    public Supervision(){
        super();
        this.setPreferredSize(new Dimension(1200, 600));
        this.setContentPane(jPanel);
        Image icono = new ImageIcon("mercadona.png").getImage();
        ImageIcon iconoR = new ImageIcon(icono.getScaledInstance(300,50,Image.SCALE_SMOOTH));
        mercadonaIcon.setIcon(iconoR);
        this.pack();

        Border border = BorderFactory.createLineBorder(Color.BLACK);
        txtCajsGrandes.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));

        Border border2 = BorderFactory.createLineBorder(Color.BLACK);
        txtCajasMedianas.setBorder(BorderFactory.createCompoundBorder(border2,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));

        Border border3 = BorderFactory.createLineBorder(Color.BLACK);
        txtCajasPequeñas.setBorder(BorderFactory.createCompoundBorder(border3,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));

        GetQuery();

    }

    private void GetQuery() {

        for (int i = 0; i <Coneccion.GetCajaConMenosTiempo().size() ; i++) {
            lblMenosTiempo.setText(Coneccion.GetCajaConMenosTiempo().get(0).substring(0,27));
            lblMaxClientes.setText(Coneccion.GetCajaConMasClientes().get(0).substring(0,15));
            lblCajaMaxTime.setText(Coneccion.GetCajaConMenosTiempo().get(Coneccion.GetCajaConMenosTiempo().size()-1).substring(0,27));
        }

        for (String o : Coneccion.GetCajaConMasProductosGrandes()){
            txtCajsGrandes.append(o + "\n");
        }

        for (String o : Coneccion.GetCajaConMasProductosMedianos()){
            txtCajasMedianas.append(o + "\n");
        }

        for (String o : Coneccion.GetCajaConMasProductosPequenios()){
            txtCajasPequeñas.append(o + "\n");
        }
    }

}
